var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var myaccount = require('./routes/myaccount');
var changepassword = require('./routes/changepassword');
var adduser = require('./routes/adduser');
var userpermission = require('./routes/userpermission');
var clientwidget = require('./routes/enduser/clientwidget');
var addclientuser = require('./routes/enduser/addclientuser');
var clientinventory = require('./routes/enduser/clientinventory');
var clientuserpermission = require('./routes/enduser/clientuserpermission');
var inventoryhold = require('./routes/enduser/inventoryhold');
var inventorytransfer = require('./routes/enduser/inventorytransfer');
var managejobs = require('./routes/enduser/managejobs');
var order = require('./routes/enduser/order');
var receipt = require('./routes/enduser/receipt');
var receiptdetails = require('./routes/enduser/receiptdetails');
var stockadjustment = require('./routes/enduser/stockadjustment');

var whwidget = require('./routes/warehouse/whwidget');
var whuserpermission = require('./routes/warehouse/whuserpermission');
var whinventory = require('./routes/warehouse/whinventory');
var warehouselist = require('./routes/warehouse/warehouselist');
var warehousedetails = require('./routes/warehouse/warehousedetails');
var whmanagejobs = require('./routes/warehouse/whmanagejobs');
var whjobdetails = require('./routes/warehouse/whjobdetails');
var whadduser = require('./routes/warehouse/whadduser');

var area = require('./routes/master/area');
var commodity = require('./routes/master/commodity');
var customersite = require('./routes/master/customersite');
var documenttype = require('./routes/master/documenttype');
var driver = require('./routes/master/driver');
var emirates = require('./routes/master/emirates');
var grade = require('./routes/master/grade');
var nationality = require('./routes/master/nationality');
var ndreason = require('./routes/master/ndreason');
var qualification = require('./routes/master/qualification');
var religion = require('./routes/master/religion');
var supplier = require('./routes/master/supplier');
var vehicle = require('./routes/master/vehicle');
var vehiclemake = require('./routes/master/vehiclemake');
var vehiclemodel = require('./routes/master/vehiclemodel');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.locals.basedir = path.join(__dirname, 'views');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/myaccount', myaccount);
app.use('/changepassword', changepassword);
app.use('/adduser', adduser);
app.use('/userpermission', userpermission);
app.use('/clientwidget', clientwidget);
app.use('/addclientuser', addclientuser);
app.use('/clientinventory', clientinventory);
app.use('/clientuserpermission', clientuserpermission);
app.use('/inventoryhold', inventoryhold);
app.use('/inventorytransfer', inventorytransfer);
app.use('/managejobs', managejobs);
app.use('/order', order);
app.use('/receipt', receipt);
app.use('/receiptdetails', receiptdetails);
app.use('/stockadjustment', stockadjustment);
app.use('/whwidget', whwidget);
app.use('/whuserpermission', whuserpermission);
app.use('/whinventory', whinventory);
app.use('/warehouselist', warehouselist);
app.use('/warehousedetails', warehousedetails);
app.use('/whmanagejobs', whmanagejobs);
app.use('/whjobdetails', whjobdetails);
app.use('/whadduser', whadduser);
app.use('/area', area);
app.use('/commodity', commodity);
app.use('/customersite', customersite);
app.use('/documenttype', documenttype);
app.use('/driver', driver);
app.use('/emirates', emirates);
app.use('/grade', grade);
app.use('/nationality', nationality);
app.use('/ndreason', ndreason);
app.use('/qualification', qualification);
app.use('/religion', religion);
app.use('/supplier', supplier);
app.use('/vehicle', vehicle);
app.use('/vehiclemake', vehiclemake);
app.use('/vehiclemodel', vehiclemodel);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
