var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('warehouse/whmanagejobs');
});

module.exports = router;
