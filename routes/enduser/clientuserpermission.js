var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('enduser/clientuserpermission');
});

module.exports = router;
