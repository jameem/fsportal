//Datatable
        $(document).ready(function () {
        $('#tblSKUDetails').DataTable();
        jQuery('#btnAddSKU').click(function (event) {
        $('#popup').modal('show');
        });
        //checkbox navigaton in table
        $('#chkHeader').change(function () {
        $('#tblSKUDetails tbody tr td input[type="checkbox"]').prop('checked', $(this).prop('checked'));
        });
        $('#tblSKUDetails tbody tr td input:checkbox').change(function () {
        $('#chkHeader').prop('checked', $('td input:checkbox:checked').length == $('td input:checkbox').length);
        });
        //checkbox navigaton in table
        $('#chkHeaderMain').change(function () {
        $('#tblAdjustmentDetails tbody tr td input[type="checkbox"]').prop('checked', $(this).prop('checked'));
        });
        $('#tblAdjustmentDetails tbody tr td input:checkbox').change(function () {
        $('#chkHeaderMain').prop('checked', $('td input:checkbox:checked').length == $('td input:checkbox').length);
        });
        //Take sku details from popup and append to the main table
        $('#btnSubmitpop').click(function () {
        $("#tblAdjustmentDetails > tbody").empty();
        var i = 0;
        $('#tblSKUDetails').find('tr').not(":first").each(function () {
        i++;
        if ($(this).find('input[type="checkbox"]').is(':checked')) {
        var sku = $(this).find('td:eq(1)').text();
        var description = $(this).find('td:eq(2)').text();
        var batch = $(this).find('td:eq(3)').text();
        var manudate = $(this).find('td:eq(4)').text();
        var expirydate = $(this).find('td:eq(5)').text();
        var stock = $(this).find('td:eq(6)').text();
        var newRow = jQuery('<tr><td><input id="checkbox' + i + '" type="checkbox" /><label for="checkbox' + i + '">Select</label></td><td>' + sku + '</td><td>' + description + '</td><td>' + batch + '</td><td>' + manudate + '</td><td>' + expirydate + '</td><td>' + stock + '</td><td><input id="txtAdjQty" type="number" class="grid-textbox" /></td><td><input id="txtTargetQty" type="number" class="grid-textbox" /></tr>');
        jQuery('#tblAdjustmentDetails').append(newRow);
        }
        });
        $('#popup').modal('hide');
        });
        //Export table Excel
        jQuery('#btnExportSKUDetails').click(function (event) {
        $('#tblSKUDetails').tableExport({ type: 'excel', escape: 'false' });
        });
        //Export table PDF
        jQuery('#btnExportSKUDetailstoPDF').click(function (event) {
        $('#tblSKUDetails').tableExport({ type: 'pdf', pdfFontSize: '7', escape: 'false' });
        });
        //Export table Excel
        jQuery('#btnExportReceiptDetails').click(function (event) {
        $('#tblAdjustmentDetails').tableExport({ type: 'excel', escape: 'false' });
        });
        //Export table PDF
        jQuery('#btnExportReceiptDetailstoPDF').click(function (event) {
        $('#tblAdjustmentDetails').tableExport({ type: 'pdf', pdfFontSize: '7', escape: 'false' });
        });
        $("#btnSubmit").click(function (e) {
        $('#Alertpopup').modal('show');
        });
        });
        $('#btnAddDetails').click(function () {
        });