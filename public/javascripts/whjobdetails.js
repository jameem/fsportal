//Datatable
      $(document).ready(function () {
      $("#lnkEdit").addClass('disabled');
      $('#example').DataTable();
      //Select table row on click
      $("#example tr").click(function (e) {
      var selected = $(this).hasClass("highlight");
      $("#example tr").removeClass("highlight");
      if (!selected)
      $(this).addClass("highlight");
      //Store selected row
      SelectedRow = $(this);
      $("#lnkEdit").removeClass('disabled');
      });
      $("#example tr").hover(function () {
      $(this).css('cursor', 'pointer');
      }, function () {
      $(this).css('cursor', 'auto');
      });
      $("#lnkEdit").click(function (e) {
      window.location.href = "Add_Warehouse.html?isedit=true";
      });
      $("#btnQuotation").click(function (e) {
      $('#Quotationpopup').modal('show');
      });
      $("#btnExprotPDF").click(function (e) {
      var printContents = document.getElementById('QuotationBody').innerHTML;
      var originalContents = document.body.innerHTML;
      document.body.innerHTML = printContents;
      window.print();
      document.body.innerHTML = originalContents;
      });
      $("#btnSendMail").click(function (e) {
      $('#Quotationpopup').modal('hide');
      $('#Alertpopup').modal('show');
      });
      jQuery('#btnAddRow').click(function (event) {
      event.preventDefault();
      var newRow = jQuery('<tr><td><input id="txtCharge" type="text" class="grid-textbox" /></td><td><input id="txtNarration" type="text" class="grid-textbox" /></td><td><input id="txtUnitPrice" type="text" class="grid-textbox" /></td><td><input id="txtRate" type="text" class="grid-textbox" /></td></tr>');
      jQuery('#tblCharges').append(newRow);
      });
      $("#btnSubmit").click(function (e) {
      $('#Quotationpopup').modal('show');
      });
      });