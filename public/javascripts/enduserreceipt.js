
      //Datatable
      $(document).ready(function () {
      $('#example').DataTable();
      //Select table row on click
      var SelectedRow;
      $("#example tr").not('thead tr').not('tfoot tr').click(function (e) {
      var selected = $(this).hasClass("highlight");
      $("#example tr").removeClass("highlight");
      if (!selected)
      $(this).addClass("highlight");
      window.open('/receiptdetails?isedit=true', '_blank');
      });
      $("#example tr").hover(function () {
      $(this).css('cursor', 'pointer');
      }, function () {
      $(this).css('cursor', 'auto');
      });
      jQuery('#btnAddRow').click(function (event) {
      event.preventDefault();
      var newRow = jQuery('<tr><td><input id="txtSKUID" type="text" class="grid-textbox"/></td><td><input id="txtDescription" type="text" class="grid-textbox" /></td><td><input id="txtTotExpQty" type="text" class="grid-textbox" /></td><td><input id="txtTotQtyRecvd" type="text" class="grid-textbox" /></td><td><input id="txtTotBM" type="text" class="grid-textbox" /></td></tr>');
      jQuery('#tblReceiptDetails').append(newRow);
      });
      $("#btnSubmitReceipt").click(function (e) {
      $('#Alertpopup').modal('show');
      $('#Quotationpopup').modal('hide');
      });
      });