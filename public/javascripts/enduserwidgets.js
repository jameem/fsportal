$(function () {
      $("#btnGotocart").click(function (e) {
            $(".my-cart-icon").click();
      });
      
      var goToCartIcon = function ($addTocartBtn) {
            var $cartIcon = $(".my-cart-icon");
            var $image = $('<img width="30px" height="30px" src="' + $addTocartBtn.data("image") + '"/>').css({ "position": "fixed", "z-index": "999" });
            $addTocartBtn.prepend($image);
            var position = $cartIcon.position();
            $('#Addtocartpopup').modal('show');
      }
      /*
      $('.my-cart-btn').myCart({
            currencySymbol: 'AED',
            classCartIcon: 'my-cart-icon',
            classCartBadge: 'my-cart-badge',
            classProductQuantity: 'my-product-quantity',
            classProductRemove: 'my-product-remove',
            classCheckoutCart: 'my-cart-checkout',
            affixCartIcon: true,
            showCheckoutModal: true,
            numberOfDecimals: 2,
            cartItems: [
                        ],
            clickOnAddToCart: function ($addTocart) {
                  goToCartIcon($addTocart);
            },
            afterAddOnCart: function (products, totalPrice, totalQuantity) {
                  console.log("afterAddOnCart", products, totalPrice, totalQuantity);
            },
            clickOnCartIcon: function ($cartIcon, products, totalPrice, totalQuantity) {
                  console.log("cart icon clicked", $cartIcon, products, totalPrice, totalQuantity);
            },
            checkoutCart: function (products, totalPrice, totalQuantity) {
                  var checkoutString = "Total Price: " + totalPrice + "\\nTotal Quantity: " + totalQuantity;
                  checkoutString += "\\n\n id \t name \t summary \t price \t quantity \t image path";
                  $.each(products, function () {
                  checkoutString += ("\\n " + this.id + " \t " + this.name + " \t " + this.summary + " \t " + this.price + " \t " + this.quantity + " \t " + this.image);
                  });
      //alert(checkoutString)
                  console.log("checking out", products, totalPrice, totalQuantity);
            },
            getDiscountPrice: function (products, totalPrice, totalQuantity) {
                  console.log("calculating discount", products, totalPrice, totalQuantity);
                  return totalPrice;
            }
      });
      */
      $("#btnSubmit").click(function (e) {
            $('#Alertpopup').modal('show');
      });
      $("#btnok").click(function (e) {
            window.location = "Index.html";
      });
      // Make the dashboard widgets sortable Using jquery UI
      $('.connectedSortable').sortable({
            placeholder: 'sort-highlight',
            connectWith: '.connectedSortable',
            handle: '.box-header, .nav-tabs',
            forcePlaceholderSize: true,
            zIndex: 999999
      });
      $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');
      $(document).on('keydown', function (e) {
            if (e.keyCode === 27) { // ESC
                  $(".simplePopupClose").click();
            }
      });
      new Morris.Donut({
            element: 'Warehouse-chart',
            data: [
            { value: 15, label: 'Warehouses' },
            { value: 10, label: 'Sub Warehouses' },
            { value: 3, label: 'Others' }
            ],
            formatter: function (x) { return x + "%" }
            }).on('click', function (i, row) {
            console.log(i, row);
      });
      new Morris.Bar({
            element: 'Stock-sumary-chart',
            data: [
            { x: 'Warehouse1', y: 3, z: 2, a: 3 },
            { x: 'Warehouse2', y: 2, z: null, a: 1 },
            { x: 'Warehouse3', y: 0, z: 2, a: 4 },
            { x: 'Warehouse4', y: 2, z: 4, a: 3 }
            ],
            xkey: 'x',
            ykeys: ['y', 'z', 'a'],
            labels: ['Y', 'Z', 'A']
            }).on('click', function (i, row) {
            console.log(i, row);
      });
      new Morris.Line({
            element: 'Stock-movement-chart',
            data: [
            { y: '2006', a: 100 },
            { y: '2007', a: 75 },
            { y: '2008', a: 50 },
            { y: '2009', a: 75 },
            { y: '2010', a: 50 },
            { y: '2011', a: 75 },
            { y: '2012', a: 100 }
            ],
            xkey: 'y',
            ykeys: ['a'],
            labels: ['Series A', 'Series B']
      });
      new Morris.Donut({
            element: 'Warehouse-occupancy-chart',
            data: [
            { value: 70, label: 'FS Occupied' },
            { value: 20, label: 'Free' },
            { value: 10, label: 'Others' }
            ],
            backgroundColor: '#ffffff',
            labelColor: '#060',
            colors: [
            '#0BA462',
            '#39B580',
            '#67C69D'
            ],
            formatter: function (x) { return x + "%" }
      });
      
});