$(document).ready(function () {
$("#btnSubmit").click(function (e) {
      $('#Alertpopup').modal('show');
      });
      // Make the dashboard widgets sortable Using jquery UI
      $('.connectedSortable').sortable({
      placeholder: 'sort-highlight',
      connectWith: '.connectedSortable',
      handle: '.box-header, .nav-tabs',
      forcePlaceholderSize: true,
      zIndex: 999999
      });
      $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');
      $("#warehousewidget").click(function (e) {
      $("#divpopcontent").empty();
      $("#poptitle").text("Warehouse");
      var a = document.getElementById('Warehouse-chart').innerHTML;
      $('#divpopcontent').append(a);
      $('#divPop').simplePopup();
      });
      $("#StockSummarywidget").click(function (e) {
      $("#divpopcontent").empty();
      $("#poptitle").text("Stock Summary");
      var a = document.getElementById('Stock-sumary-chart').innerHTML;
      $('#divpopcontent').append(a);
      $('#divPop').simplePopup();
      });
      $("#StockMovementwidget").click(function (e) {
      $("#divpopcontent").empty();
      $("#poptitle").text("Stock Movement");
      var a = document.getElementById('Stock-movement-chart').innerHTML;
      $('#divpopcontent').append(a);
      $('#divPop').simplePopup();
      });
      $("#WarehouseOccupancewidget").click(function (e) {
      $("#divpopcontent").empty();
      $("#poptitle").text("Warehouse Occupance");
      var a = document.getElementById('Warehouse-occupancy-chart').innerHTML;
      $('#divpopcontent').append(a);
      $('#divPop').simplePopup();
      });
      $(document).on('keydown', function (e) {
      if (e.keyCode === 27) { // ESC
      $(".simplePopupClose").click();
      }
      });
      new Morris.Donut({
      element: 'Warehouse-chart',
      data: [
      { value: 15, label: 'Warehouses' },
      { value: 10, label: 'Sub Warehouses' },
      { value: 3, label: 'Others' }
      ],
      formatter: function (x) { return x + "%" }
      }).on('click', function (i, row) {
      console.log(i, row);
      });
      new Morris.Bar({
      element: 'Stock-sumary-chart',
      data: [
      { x: 'Warehouse1', y: 3, z: 2, a: 3 },
      { x: 'Warehouse2', y: 2, z: null, a: 1 },
      { x: 'Warehouse3', y: 0, z: 2, a: 4 },
      { x: 'Warehouse4', y: 2, z: 4, a: 3 }
      ],
      xkey: 'x',
      ykeys: ['y', 'z', 'a'],
      labels: ['Y', 'Z', 'A']
      }).on('click', function (i, row) {
      console.log(i, row);
      });
      new Morris.Line({
      element: 'Stock-movement-chart',
      data: [
      { y: '2006', a: 100},
      { y: '2007', a: 75},
      { y: '2008', a: 50},
      { y: '2009', a: 75},
      { y: '2010', a: 50},
      { y: '2011', a: 75},
      { y: '2012', a: 100}
      ],
      xkey: 'y',
      ykeys: ['a'],
      labels: ['Series A', 'Series B']
      });
      new Morris.Donut({
      element: 'Warehouse-occupancy-chart',
      data: [
      { value: 70, label: 'FS Occupied' },
      { value: 20, label: 'Free' },
      { value: 10, label: 'Others' }
      ],
      backgroundColor: '#ffffff',
      labelColor: '#060',
      colors: [
      '#0BA462',
      '#39B580',
      '#67C69D'
      ],
      formatter: function (x) { return x + "%" }
      });
    });