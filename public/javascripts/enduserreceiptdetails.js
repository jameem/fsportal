$(document).ready(function () {
        $('#tblSKUDetails').DataTable();
        $('#flExcel').change(Upload);
        function Upload() {
        var fileUpload = document.getElementById("flExcel");
        var regex = /^([a-zA-Z0-9\\s_\\.\-:])+(.csv|.txt)$/;
        if (regex.test(fileUpload.value.toLowerCase())) {
        if (typeof (FileReader) != "undefined") {
        $("#tblReceiptDetails > tbody").empty();
        var reader = new FileReader();
        reader.onload = function (e) {
        var rows = e.target.result.split("\\n");
        for (var i = 1; i < rows.length - 1; i++) {
        var cells = rows[i].split(",");
        var sku = cells[0];
        var description = cells[1];
        var newRow = jQuery('<tr><td><input id="checkbox' + i + '" type="checkbox" /><label for="checkbox' + i + '">Select</label></td><td>' + sku + '</td><td>' + description + '</td><td><input id="txtBatch" type="text" class="grid-textbox" /></td><td><input id="txtExpiryDate" type="date" class="grid-textbox" /></td><td><input id="txtManuDate" type="date" class="grid-textbox" /></td><td><input id="txtTotExpQty" type="number" class="grid-textbox" /></td><td><input id="txtTotQtyRecieved" type="number" class="grid-textbox" /></td></tr>');
        jQuery('#tblReceiptDetails').append(newRow);
        }
        }
        reader.readAsText(fileUpload.files[0]);
        } else {
        alert("This browser does not support HTML5.");
        }
        } else {
        alert("Please upload a valid CSV file.");
        }
        }
    $('#example').DataTable();
        //Select table row on click
        var SelectedRow;
        $("#example tr").not('thead tr').not('tfoot tr').click(function (e) {
        var selected = $(this).hasClass("highlight");
        $("#example tr").removeClass("highlight");
        if (!selected)
        $(this).addClass("highlight");
        //Store selected row
        SelectedRow = $(this);
        $('#Quotationpopup').modal('show');
        });
        $("#example tr").hover(function () {
        $(this).css('cursor', 'pointer');
        }, function () {
        $(this).css('cursor', 'auto');
        });
        jQuery('#btnAddSKU').click(function (event) {
        $('#SKUpopup').modal('show');
        });
        $("#btnSubmitReceipt").click(function (e) {
        $('#Alertpopup').modal('show');
        });
        //checkbox navigaton in table
        $('#Checkboxheader').change(function () {
        $('#tblSKUDetails tbody tr td input[type="checkbox"]').prop('checked', $(this).prop('checked'));
        });
        $('td input:checkbox').change(function () {
        $('#Checkboxheader').prop('checked', $('td input:checkbox:checked').length == $('td input:checkbox').length);
        });
        //checkbox navigaton in table
        $('#chkHeaderMain').change(function () {
        $('#tblReceiptDetails tbody tr td input[type="checkbox"]').prop('checked', $(this).prop('checked'));
        });
        $('#tblReceiptDetails tbody tr td input:checkbox').change(function () {
        $('#chkHeaderMain').prop('checked', $('td input:checkbox:checked').length == $('td input:checkbox').length);
        });
        //Export table Excel
        jQuery('#btnExportSKUDetails').click(function (event) {
        $('#tblSKUDetails').tableExport({ type: 'excel', escape: 'false' });
        });
        //Export table PDF
        jQuery('#btnExportSKUDetailstoPDF').click(function (event) {
        $('#tblSKUDetails').tableExport({ type: 'pdf', pdfFontSize: '7', escape: 'false' });
        });
        //Export table Excel
        jQuery('#btnExportReceiptDetails').click(function (event) {
        $('#tblReceiptDetails').tableExport({ type: 'excel', escape: 'false' });
        });
        //Export table PDF
        jQuery('#btnExportReceiptDetailstoPDF').click(function (event) {
        $('#tblReceiptDetails').tableExport({ type: 'pdf', pdfFontSize: '7', escape: 'false' });
        });
        //Take sku details from popup and append to the main table
        $('#btnSKUSubmit').click(function () {
        $("#tblReceiptDetails > tbody").empty();
        var i = 0;
        $('#tblSKUDetails').find('tr').not(":first").each(function () {
        i++;
        if ($(this).find('input[type="checkbox"]').is(':checked')) {
        var sku = $(this).find('td:eq(1)').text();
        var description = $(this).find('td:eq(2)').text();
        var newRow = jQuery('<tr><td><input id="checkbox' + i + '" type="checkbox" /><label for="checkbox' + i + '">Select</label></td><td>' + sku + '</td><td>' + description + '</td><td><input id="txtBatch" type="text" class="grid-textbox" /></td><td><input id="txtExpiryDate" type="date" class="grid-textbox" /></td><td><input id="txtManuDate" type="date" class="grid-textbox" /></td><td><input id="txtTotExpQty" type="number" class="grid-textbox" /></td><td><input id="txtTotQtyRecieved" type="number" class="grid-textbox" /></td></tr>');
        jQuery('#tblReceiptDetails').append(newRow);
        }
        });
        });
        });
    