$(document).ready(function () {
    $(document).on('keydown', function (e) {
      if (e.keyCode === 27) { // ESC
        $(".simplePopupClose").click();
      }
    });
    new Morris.Donut({
      element: 'Warehouse-chart',
      data: [
      { value: 15, label: 'Warehouses' },
      { value: 10, label: 'Sub Warehouses' },
      { value: 3, label: 'Others' }
      ],
      formatter: function (x) { return x + "%" }
      }).on('click', function (i, row) {
      console.log(i, row);
      });
      new Morris.Bar({
      element: 'Stock-sumary-chart',
      data: [
      { x: 'Warehouse1', y: 3, z: 2, a: 3 },
      { x: 'Warehouse2', y: 2, z: null, a: 1 },
      { x: 'Warehouse3', y: 0, z: 2, a: 4 },
      { x: 'Warehouse4', y: 2, z: 4, a: 3 }
      ],
      xkey: 'x',
      ykeys: ['y', 'z', 'a'],
      labels: ['Y', 'Z', 'A']
      }).on('click', function (i, row) {
      console.log(i, row);
      });
      new Morris.Line({
      element: 'Stock-movement-chart',
      data: [
      { y: '2006', a: 100 },
      { y: '2007', a: 75 },
      { y: '2008', a: 50 },
      { y: '2009', a: 75 },
      { y: '2010', a: 50 },
      { y: '2011', a: 75 },
      { y: '2012', a: 100 }
      ],
      xkey: 'y',
      ykeys: ['a'],
      labels: ['Series A', 'Series B']
      });
      new Morris.Donut({
      element: 'Warehouse-occupancy-chart',
      data: [
      { value: 70, label: 'FS Occupied' },
      { value: 20, label: 'Free' },
      { value: 10, label: 'Others' }
      ],
      backgroundColor: '#ffffff',
      labelColor: '#060',
      colors: [
      '#0BA462',
      '#39B580',
      '#67C69D'
      ],
      formatter: function (x) { return x + "%" }
      });
      //Clicking charts
      $("#warehousewidget").click(function (e) {
      $("#popuptitle").text("Warehouse");
      $("#Warehouse-pop").show();
      $("#Stock-movement-chart-pop").hide();
      $("#Warehouse-occuppancy-chart-pop").hide();
      $("#Stock-sumary-chart-pop").hide();
      $('#popup').modal('show');
      $('#popup').on('shown.bs.modal', function (e) {
      var myChart = new Morris.Donut({
      element: 'Warehouse-pop',
      data: [
      { value: 15, label: 'Warehouses' },
      { value: 10, label: 'Sub Warehouses' },
      { value: 3, label: 'Others' }
      ],
      formatter: function (x) { return x + "%" }
      }).on('click', function (i, row) {
      console.log(i, row);
      });
      });
      });
      $("#StockSummarywidget").click(function (e) {
      $("#popuptitle").text("Stock Summary");
      $("#Warehouse-pop").hide();
      $("#Stock-movement-chart-pop").hide();
      $("#Warehouse-occuppancy-chart-pop").hide();
      $("#Stock-sumary-chart-pop").show();
      $('#popup').on('shown.bs.modal', function (e) {
      var myChart = new Morris.Bar({
      element: 'Stock-sumary-chart-pop',
      data: [
      { x: 'Warehouse1', y: 3, z: 2, a: 3 },
      { x: 'Warehouse2', y: 2, z: null, a: 1 },
      { x: 'Warehouse3', y: 0, z: 2, a: 4 },
      { x: 'Warehouse4', y: 2, z: 4, a: 3 }
      ],
      xkey: 'x',
      ykeys: ['y', 'z', 'a'],
      labels: ['Y', 'Z', 'A']
      }).on('click', function (i, row) {
      console.log(i, row);
      });
      });
      $('#popup').modal('show');
      });
      $("#StockMovementwidget").click(function (e) {
      $("#popuptitle").text("Stock Movement");
      $("#Warehouse-pop").hide();
      $("#Stock-movement-chart-pop").show();
      $("#Stock-sumary-chart-pop").hide();
      $("#Warehouse-occuppancy-chart-pop").hide();
      $('#popup').on('shown.bs.modal', function (e) {
      var myChart = new Morris.Line({
      element: 'Stock-movement-chart-pop',
      data: [
      { y: '2006', a: 100 },
      { y: '2007', a: 75 },
      { y: '2008', a: 50 },
      { y: '2009', a: 75 },
      { y: '2010', a: 50 },
      { y: '2011', a: 75 },
      { y: '2012', a: 100 }
      ],
      xkey: 'y',
      ykeys: ['a'],
      labels: ['Series A', 'Series B']
      });
      });
      $('#popup').modal('show');
      });
      $("#WarehouseOccupancewidget").click(function (e) {
      $("#popuptitle").text("Warehouse Occuppancy");
      $("#Warehouse-pop").hide();
      $("#Stock-movement-chart-pop").hide();
      $("#Stock-sumary-chart-pop").hide();
      $("#Warehouse-occuppancy-chart-pop").show();
      $('#popup').on('shown.bs.modal', function (e) {
      var myChart = new Morris.Donut({
      element: 'Warehouse-occuppancy-chart-pop',
      data: [
      { value: 70, label: 'FS Occupied' },
      { value: 20, label: 'Free' },
      { value: 10, label: 'Others' }
      ],
      backgroundColor: '#ffffff',
      labelColor: '#060',
      colors: [
      '#0BA462',
      '#39B580',
      '#67C69D'
      ],
      formatter: function (x) { return x + "%" }
      });
      });
      $('#popup').modal('show');
      });

      //Calendar
      $('#calendar').fullCalendar({
      header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,basicWeek,basicDay'
      },
      defaultDate: '2017-05-12',
      navLinks: true, // can click day/week names to navigate views
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: [
      {
      title: 'All Day Event',
      start: '2017-05-01'
      },
      {
      title: 'Long Event',
      start: '2017-05-07',
      end: '2017-05-10'
      },
      {
      id: 999,
      title: 'Repeating Event',
      start: '2017-05-09T16:00:00'
      },
      {
      id: 999,
      title: 'Repeating Event',
      start: '2017-05-16T16:00:00'
      },
      {
      title: 'Conference',
      start: '2017-05-11',
      end: '2017-05-13'
      },
      {
      title: 'Meeting',
      start: '2017-05-12T10:30:00',
      end: '2017-05-12T12:30:00'
      },
      {
      title: 'Lunch',
      start: '2017-05-12T12:00:00'
      },
      {
      title: 'Meeting',
      start: '2017-05-12T14:30:00'
      },
      {
      title: 'Happy Hour',
      start: '2017-05-12T17:30:00'
      },
      {
      title: 'Dinner',
      start: '2017-05-12T20:00:00'
      },
      {
      title: 'Birthday Party',
      start: '2017-05-13T07:00:00'
      },
      {
      title: 'Click for Google',
      url: 'http://google.com/',
      start: '2017-05-28'
      }
      ]
      });
      });
    // Make the dashboard widgets sortable Using jquery UI
      $('.connectedSortable').sortable({
      placeholder: 'sort-highlight',
      connectWith: '.connectedSortable',
      handle: '.box-header, .nav-tabs',
      forcePlaceholderSize: true,
      zIndex: 999999
      });
      $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');